<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Dans un tableau, stocker les différentes formes juridique de société possible.
    // En fonction du chiffre d'affaire contenu dans $ca, afficher plutôt tel ou tel forme juridique
    // Forme : ME, EIRL, SASU, SARL, GAEC, EARL, SAS
    
    $ca = rand(0,1000000)
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
        $tableau = ["ME", "EIRL", "SASU", "SARL", "GAEC", "EARL", "SAS"];

        echo "Chiffre d'affaire est de : $ca <br>";

        if($ca <=100000){
            echo $tableau[0];
        }elseif($ca <=200000){
            echo $tableau[1];
        }elseif($ca <=300000){
            echo $tableau[2];
        }elseif($ca <=400000){
            echo $tableau[3];
        }elseif($ca <=500000){
            echo $tableau[4];
        }elseif($ca <=600000){
            echo $tableau[5];
        }else{
            echo $tableau[6];
        }
?>
    
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>