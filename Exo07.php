<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Faites 2 tableaux : un avec 5 prénoms de garçons et un avec 5 prénoms de filles
    // En fonction du chiffre que choisit $x, afficher les 5 prénoms de garçon ou sinon les 5 prénoms de filles
    
    $x = rand(0,1);
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
    
    $tab = ["moi", "jeje", "charly", "dam", "pierre"];
    $tab2 = ["ange", "rosa", "pris", "nath", "cindy"];

    if($x == 0){
        foreach($tab as $garcon){
            echo "$garcon <br>";
        }
    }else{
        foreach($tab2 as $fille){
            echo "$fille <br>";
        }
    }
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>