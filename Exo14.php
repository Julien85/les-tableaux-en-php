<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Utiliser la fonction array_pop pour placer "Poire" dans la variable fruit
    // https://www.php.net/manual/fr/function.array-pop.php
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php

    $liste = ["orange", "banane", "Pomme", "Poire"];
    $fruit = array_pop ($liste);

    echo $fruit;

    
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>
