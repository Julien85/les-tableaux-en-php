<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Créer un tableau avec tous les mois de l'année. Faites une boucle pour afficher : Janvier est le mois numéro 1.
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
        $tableau = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"];
    
        for($i=0; $i < count ($tableau); $i++){
            echo "$tableau[$i] est le mois numéro " . ($i+1) . '<br>' ;
        }
    ?> 
    <!-- écrire le code avant ce commentaire -->

</body>
</html>