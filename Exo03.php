<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Faites un tableau contenant les communes de la Communauté de Commune de Loir Luce Berce qui sont: 
    // Loir en vallée, La Chartre sur le loir, Lhomme, Marçon, Beaumont sur Dême et Chahaignes 
    // A l'aide de la boucle FOR, afficher la liste des communes dans un h different à chaque fois
    // <h1>Loir en Vallée</h1>, <h2>La Chartres sur le loir</h2>, ...
    // Trouver le hack pour que Loir en vallée soit bien un h1 et pas un h2 
    // vous n'avez le droit qu'à deux variables : celle du tableau et celle de la boucle for
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
   <?php

    $tableau = [" ", "Loir en vallée", "La chartre sur le loir", "Lhomme", "Marcon", "Beaumont sur Deme", "Chahaignes"];

    for($i=0; $i < count($tableau); $i++){
        echo '<h' . $i . '>' . $tableau[$i] .'</h' . $i . '>';
    }

?>
    
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>