<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Faites un tableau contenant 3 fruits, faites un autre tableau contenant les calories de chaque fruits
    // Avec une fonction de PHP, faites un assemblage des deux tableaux et calculez les calories
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    
    <?php
        $fruit = ["clemetine", "pomme", "poire"];
        $calories = [12, 15, 10];

        $i = array_combine($fruit, $calories);

        foreach($i as $clef => $valeur){
            echo $clef . ' fait ' . $valeur . '<br>' . '<br>';
        }

        $j = array_sum($calories);

        echo 'total des calories des fruits font : ' . $j
?>
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>
